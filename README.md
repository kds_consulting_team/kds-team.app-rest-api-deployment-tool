# KBC rest api deployment tool

Application that allows to "apify" any KBC Python transformation. 

The only limitation on the transformation design is using relative paths for `data/` folder interactions.
 e.g. use `in/tables` rather than `/data/in/tables`.
 
 ## Configuration
 
 ### Source Transformation
 
Source transformation that will be converted to API.
**Parameters**
- Source transformation bucket ID 
    
    Can be found in the transformation URL: https://connection.{region}.keboola.com/admin/projects/{pid}/transformations/bucket/{BUCKET_ID}}/transformation/{TRANSFORMATION_ID}

- Source transformation ID
    
    Can be found in the transformation URL: https://connection.{region}.keboola.com/admin/projects/{pid}/transformations/bucket/{BUCKET_ID}}/transformation/{TRANSFORMATION_ID}

- Required Python Packages

    Comma separated list of required Python packages, not available by default in the Python distribution.

### AWS config

AWS config parameters.
- AWS API Key ID
- AWS API Key Secret
- AWS S3 bucket name

    An existing S3 bucket name that will be used for lambda function package staging.

- AWS Region

### Lambda function parameters

Parameters of the lambda function

- Available memory (MB)
- Lambda function URL path.

    The resulting function will be available at https://{APP_ID}.execute-api.{REGION}.amazonaws.com/kbc/{URL_PATH}/

### Function Authorized API Keys

List of API Keys that will be authorized for the endpoint use. The keys are stored in the environment variables of the Lambda function.

## Development
 
This example contains runnable container with simple unittest. For local testing it is useful to include `data` folder in the root
and use docker-compose commands to run the container or execute tests. 

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path:
```yaml
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
```

Clone this repository, init the workspace and run the component with following command:

```
git clone https://bitbucket.org:kds_consulting_team/kbc-python-template.git my-new-component
cd my-new-component
docker-compose build
docker-compose run --rm dev
```

Run the test suite and lint check using this command:

```
docker-compose run --rm test
```

# Integration

For information about deployment and integration with KBC, please refer to the [deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/) 