**NOTE:** The resulting API endpoint will be displayed in the **job log**.

![log](https://i.ibb.co/B2jqvWV/log.png)

Example Request:

```bash
curl -X POST \
  https://{APP_ID}.execute-api.{REGION}.amazonaws.com/kbc/{URL_PATH}/ \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Authorization: 3QoRjpZUsLx7' \
  -H 'Content-Type: application/json' \

  -d '{"table1.csv": 
{"columns":["B Bid","B Ask","P Bid","P Ask"],
"data":[
  [64.4,64.9,81.3,82.3],
  [0,0,0,0]
]},
"table2.csv": {"columns":["start_date","end_date"], "data":[["11/30/2018","12/31/2021"]]}
}
'
```