'''
Template Component main class.

'''

import json
import logging
import os
import sys
import urllib
from distutils import dir_util

import boto3
import cfn_tools
import requests
import subprocess
from kbc.env_handler import KBCEnvHandler

# configuration variables
PACKAGES = 'packages'
REGION = 'region'
BUCKET_ID = 'bucket_id'
TRANSFORMATION_ID = 'transformation_id'
STORAGE_TOKEN = '#storage_token'

# aws params
KEY_AWS_PARAMS = 'aws_parameters'
KEY_AWS_API_KEY_ID = 'api_key_id'
KEY_AWS_API_KEY_SECRET = '#api_key_secret'
KEY_AWS_REGION = 'aws_region'
KEY_AWS_S3_BUCKET = 's3_bucket'

# lambda params
KEY_LAMBDA_CFG = 'lambda_config'
KEY_CF_STACK_NAME = 'cf_stack_name'
KEY_URL_PATH = 'url_path'
KEY_MEMORY_AVAILABLE = 'memory_available'
KEY_API_KEYS = 'api_keys'
KEY_SECRET = 'secret'
KEY_NAME = 'name'

KEY_SOURCE_TR = 'source_transformation'
# #### Keep for debug
KEY_STDLOG = 'stdlogging'
KEY_DEBUG = 'debug'

MANDATORY_PARS = [KEY_LAMBDA_CFG, KEY_AWS_PARAMS, KEY_SOURCE_TR]
MANDATORY_IMAGE_PARS = []

# current working dir
PAR_DIRPATH = os.path.dirname(os.path.abspath(os.path.join(os.path.abspath(__file__), os.pardir)))
TMP_FOLDER_PATH = os.path.join('/tmp')
RESOURCES_FOLDER_PATH = os.path.join(PAR_DIRPATH, 'resources')

PAR_SECRET_MGR_TAG = 'keboola_tr2lambda'

APP_VERSION = '0.0.1'


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS)
        # override debug from config
        if self.cfg_params.get(KEY_DEBUG):
            debug = True

        log_level = logging.DEBUG if debug else logging.INFO
        # setup GELF if available
        if os.getenv('KBC_LOGGER_ADDR', None):
            self.set_gelf_logger(log_level)
        else:
            self.set_default_logger(log_level)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config()
            self.validate_parameters(self.cfg_params[KEY_LAMBDA_CFG],
                                     [KEY_MEMORY_AVAILABLE, KEY_URL_PATH, KEY_API_KEYS], KEY_LAMBDA_CFG)
            self.validate_parameters(self.cfg_params[KEY_AWS_PARAMS],
                                     [KEY_AWS_API_KEY_ID, KEY_AWS_API_KEY_SECRET, KEY_AWS_REGION, KEY_AWS_S3_BUCKET],
                                     KEY_AWS_PARAMS)
            self.validate_parameters(self.cfg_params[KEY_SOURCE_TR],
                                     [BUCKET_ID, PACKAGES, TRANSFORMATION_ID],
                                     KEY_AWS_PARAMS)
        except ValueError as e:
            logging.exception(e)
            exit(1)

        self.packages_string = self.cfg_params[KEY_SOURCE_TR][PACKAGES]
        self.bucket_id = self.cfg_params[KEY_SOURCE_TR][BUCKET_ID]
        self.transformation_id = self.cfg_params[KEY_SOURCE_TR][TRANSFORMATION_ID]

        # init variables
        self.storage_token = self.get_storage_token()
        self.stack_id = os.environ["KBC_STACKID"]

    def run(self):
        '''
        Main execution code
        '''

        # get the list of packages, split it to a list, and then write it out to txt
        packages_list = [p.strip() for p in self.packages_string.split(",")]

        lambda_pars = self.cfg_params[KEY_LAMBDA_CFG]
        aws_pars = self.cfg_params[KEY_AWS_PARAMS]

        logging.info("Preparing package folder.")
        tr_name, tr_id = self.prepare_package_folder(packages_list)

        logging.info("Setting up AWS cli parameters.")
        self.setup_aws_config(aws_pars)

        logging.info("Generating CloudFormation template.")
        self.prepare_cf_template(lambda_pars, aws_pars, tr_name, tr_id)

        logging.info("Deploying SAM package.")
        self.build_n_deploy_sam_package(lambda_pars, aws_pars)

        logging.info("Getting generated endpoint URL")
        client = boto3.client('apigateway')
        apis = client.get_rest_apis()
        api_id = [r['id'] for r in apis['items'] if r['name'] == lambda_pars[KEY_CF_STACK_NAME]]
        api_url = f'https://{api_id[0]}.execute-api.{aws_pars[KEY_AWS_REGION]}' \
                  f'.amazonaws.com/kbc/{lambda_pars[KEY_URL_PATH]}/'

        logging.info(f"Api created! Your function is available at {api_url}")

        # get created stack
        client = boto3.client('cloudformation')
        stacks = client.list_stacks(
            StackStatusFilter=['CREATE_COMPLETE', 'UPDATE_COMPLETE', 'UPDATE_ROLLBACK_COMPLETE', 'IMPORT_COMPLETE',
                               'IMPORT_ROLLBACK_COMPLETE'])
        stack_id = [r['StackId'] for r in stacks['StackSummaries'] if r['StackName'] == lambda_pars[KEY_CF_STACK_NAME]]
        stack_url = f'https://{aws_pars[KEY_AWS_REGION]}.console.aws.amazon.com/cloudformation/home?'
        stack_query = urllib.parse.quote(f'region={aws_pars[KEY_AWS_REGION]}#/stacks/outputs?stackId={stack_id[0]}')

        logging.info(f"You can also review the created stack {lambda_pars[KEY_CF_STACK_NAME]} "
                     f"in the console: {stack_url + stack_query}")

    def prepare_package_folder(self, packages_list):
        requirements_file = os.path.join(TMP_FOLDER_PATH, 'requirements.txt')
        os.makedirs(TMP_FOLDER_PATH, exist_ok=True)
        # copy template files to tmp stage
        dir_util.copy_tree(RESOURCES_FOLDER_PATH, TMP_FOLDER_PATH, update=1)

        with open(requirements_file, 'a') as f:
            for row, item in enumerate(packages_list):
                if row < len(packages_list) - 1:
                    f.write("%s\n" % item)
                elif row == len(packages_list) - 1:
                    f.write("%s" % item)

        transformation_config = self._get_configuration(
            stack_id=self.stack_id,
            bucket_id=self.bucket_id,
            transformation_id=self.transformation_id,
            storage_token=self.storage_token)

        query = transformation_config['configuration']['queries'][0]

        python_tr_file = os.path.join(TMP_FOLDER_PATH, 'tr2lambda', 'keboola_python_tr.py')

        with open(python_tr_file, 'w') as f:
            f.write(query)
        return transformation_config['name'], transformation_config['id']

    def prepare_cf_template(self, lambda_pars, aws_pars, tr_name, tr_id):
        with open(os.path.join(TMP_FOLDER_PATH, "template.yaml"), 'r') as input:
            template = cfn_tools.load_yaml(input)
        if lambda_pars.get(KEY_CF_STACK_NAME):
            stack_name = KEY_CF_STACK_NAME
        else:
            nm_norm = tr_name.lower().replace(' ', '-')
            stack_name = '-'.join(['keboola-tr', nm_norm, tr_id])
            lambda_pars[KEY_CF_STACK_NAME] = stack_name

        gtw_props = template['Resources']['KeboolaBasicAWSApiGateway']['Properties']
        gtw_props['Name'] = stack_name

        lmbd_props = template['Resources']['KeboolaTr2Lambda']['Properties']
        # set api keys env
        lmbd_props['Environment']['Variables']['api_keys'] = json.dumps(lambda_pars[KEY_API_KEYS])

        lmbd_props['MemorySize'] = lambda_pars[KEY_MEMORY_AVAILABLE]
        lmbd_props['Events']['Tr2Lambda']['Properties']['Path'] = '/' + str(lambda_pars[KEY_URL_PATH])

        output_pr = template['Outputs']['KeboolaBasicAWSApiGateway']
        output_pr['Value']['Fn::Sub'] = output_pr['Value']['Fn::Sub'] % {'url_path': lambda_pars[KEY_URL_PATH]}

        # set export keys
        stackid = stack_name.replace(' ', '')
        apigtw_export_nm = template['Outputs']['KeboolaBasicAWSApiGatewayRestApiId']['Export']['Name']
        template['Outputs']['KeboolaBasicAWSApiGatewayRestApiId']['Export']['Name'] = apigtw_export_nm.replace(
            'RestApiId', stackid)

        root_res = template['Outputs']['KeboolaBasicAWSApiGatewayyRootResourceId']
        root_res['Export']['Name'] = root_res['Export']['Name'].replace(
            'RootResourceId', stackid)

        with open(os.path.join(TMP_FOLDER_PATH, "template.yaml"), 'w') as tmpl_out:
            tmpl_out.write(cfn_tools.dump_yaml(template))

    def setup_aws_config(self, aws_params):
        # set HOME just in case
        os.environ["HOME"] = str(TMP_FOLDER_PATH)
        cfg__dir_path = os.path.join(TMP_FOLDER_PATH, '.aws')
        os.makedirs(cfg__dir_path, exist_ok=True)
        creds = f"""
        [default]
        aws_access_key_id = {aws_params[KEY_AWS_API_KEY_ID]}
        aws_secret_access_key = {aws_params[KEY_AWS_API_KEY_SECRET]}
        """
        with open(os.path.join(cfg__dir_path, 'credentials'), 'w+') as credentials:
            credentials.write(creds)

        cfg = f"""
        [default]
        region = {aws_params[KEY_AWS_REGION]}
        output = json
        """
        with open(os.path.join(cfg__dir_path, 'config'), 'w+') as config:
            config.write(cfg)

    def build_n_deploy_sam_package(self, lambda_pars, aws_pars):
        os.chdir(TMP_FOLDER_PATH)
        logging.info("Building sam package to S3.")
        # sam build
        process = subprocess.run(['sam', 'build'],
                                 capture_output=True,
                                 universal_newlines=True)
        if process.returncode > 0:
            raise Exception(f"Building sam package failed: {process.stderr}")
        else:
            logging.info(process.stdout)

        logging.info("Packaging and uploading to S3...")
        # sam package
        process = subprocess.run(['sam', 'package', '--output-template-file', 'packaged.yaml', '--s3-bucket',
                                  aws_pars[KEY_AWS_S3_BUCKET]],
                                 capture_output=True,
                                 universal_newlines=True)
        if process.returncode > 0:
            raise Exception(f"Running sam package failed: {process.stderr}")
        else:
            logging.info(process.stdout)

        logging.info("Deploying the CloudFormation stack..")
        # sam deploy
        process = subprocess.run(['sam', 'deploy', '--template-file', 'packaged.yaml', '--stack-name',
                                  lambda_pars[KEY_CF_STACK_NAME], '--capabilities', 'CAPABILITY_IAM'],
                                 capture_output=True,
                                 universal_newlines=True)
        if process.returncode > 0:
            raise Exception(f"Running sam deploy failed: {process.stderr}")
        else:
            logging.info(process.stdout)

    def _get_configuration(self, stack_id, bucket_id, transformation_id, storage_token):

        base_url = 'https://' + stack_id + '/v2/storage/'

        url_configuration = os.path.join(
            base_url, f'components/transformation/configs/{bucket_id}/rows/{transformation_id}')
        header_configuration = {
            'x-storageapi-token': storage_token
        }

        req_configuration = requests.get(
            url_configuration, headers=header_configuration)
        sc_configuration, js_configuration = req_configuration.status_code, req_configuration.json()

        if sc_configuration != 200:

            raise Exception(
                f"Could not obtain configuration of bucket {bucket_id} for transformation id {transformation_id}.")

        else:

            return js_configuration


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug = sys.argv[1]
    else:
        debug = False
    try:
        comp = Component()
        comp.run()
    except Exception as e:
        logging.exception(e)
        exit(1)
