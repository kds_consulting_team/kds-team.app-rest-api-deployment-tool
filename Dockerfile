FROM linuxbrew/debian
ENV PYTHONIOENCODING utf-8
RUN brew tap aws/tap; \
brew install awscli aws-sam-cli

# install gcc to be able to build packages - e.g. required by regex, dateparser, also required for pandas
USER root
RUN apt-get update && apt-get install -y build-essential && apt-get install -y python3-pip
RUN pip3 install flake8

COPY . /code/
RUN pip3 install -r /code/requirements.txt

WORKDIR /code/

ENV HOME /tmp

CMD ["python3", "-u", "/code/src/component.py"]
