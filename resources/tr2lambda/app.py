import base64
import binascii
import gzip
import json
import os
import sys
import ast

import pandas as pd
import shutil

KEY_API_KEYS_ENV = 'api_keys'


class LambdaException(Exception):
    """
    Lambda ex
    """


def check_authorization(event):
    # api keys are stored in environment varialble
    keys_dict = ast.literal_eval(os.getenv(KEY_API_KEYS_ENV))
    keys = [k['#secret'] for k in keys_dict]
    if not event['headers'].get('Authorization') or event['headers'].get('Authorization') not in keys:
        return False
    else:
        return True


def cleanup_tmp_folder():
    if not os.path.exists('/tmp'):
        return
    for root, dirs, files in os.walk('/tmp'):
        for name in files:
            print(os.path.join(root, name))
            os.remove(os.path.join(root, name))
        for name in dirs:
            print(os.path.join(root, name))
            shutil.rmtree(os.path.join(root, name))


def prepare_common_interface(body):
    # cleanup tmp folder (may be shared)
    cleanup_tmp_folder()
    # build folder structure
    if not os.path.exists('/tmp'):
        os.makedirs('/tmp')
    if not os.path.exists('/tmp/in/tables'):
        os.makedirs('/tmp/in/tables')
    if not os.path.exists('/tmp/out/tables'):
        os.makedirs('/tmp/out/tables')

    body = json.loads(body)
    error = ''
    for key in body:
        try:
            tb = pd.read_json(json.dumps(body[key]), orient='split')
            tb.to_csv('/tmp/in/tables/' + key, index=False)
        except Exception as e:
            error += F"Parsing of table definition '{key}' failed with error: '{str(e)}'"
    if error:
        raise LambdaException(error)
    os.chdir('/tmp')


def build_response_body():
    body = dict()
    for filename in os.listdir('/tmp/out/tables'):
        tb = pd.read_csv(os.path.join('/tmp/out/tables', filename))
        dct = tb.to_dict(orient='split')
        dct.pop('index', None)
        body = {**body, **{filename: dct}}
    return json.dumps(body)


def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc:
        https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    try:
        print('Authorization')
        if not check_authorization(event):
            return {"body": json.dumps('Invalid token or missing Authorization header!'),
                    "statusCode": 401}

        print('Collecting request')
        c_type = event['headers']['Content-Type']
        # c_enc = event['headers'].get('Content-Encoding')

        print('Parsing input')
        if c_type == 'application/x-gzip':
            body = gzip.decompress(binascii.a2b_base64(event['body'].encode('utf-8'))).decode()
        else:
            body = event['body']

        print("Preparing environment")
        prepare_common_interface(body)

        print("Executing python transformation function")
        try:
            curr_directory = os.path.dirname(os.path.realpath(__file__))
            script = open(os.path.join(curr_directory, 'keboola_python_tr.py')).read()
            exec(script)
        except Exception as e:
            raise LambdaException(F"Excecution of the Python Transformation code failed with error: '{str(e)}'")

        body = build_response_body()
        if sys.getsizeof(body) > 5000000:
            body = gzip.compress(bytes(body, 'utf-8'))
            body = json.dumps(str(base64.b64encode(body), 'utf-8'))
            response = {"body": body, "statusCode": 200,
                        "isBase64Encoded": True,
                        "headers": {
                            "Content-Encoding": "gzip"
                        }}
        else:
            response = {"body": body, "statusCode": 200}

        return response
    except LambdaException as e:
        print(e)
        return {"body": json.dumps({"error": str(e)}),
                "statusCode": 400
                }
    except Exception as e:
        # Send some context about this error to Lambda Logs
        print(e)
        return {"body": json.dumps({"error": str(e)}),
                "statusCode": 500
                }
